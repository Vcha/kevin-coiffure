<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Kevin Coiffure</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <link rel="stylesheet" href="style.css">

  </head>
  <body>

    <div class="container-fluid">
      <div class="row">
        <div class="col s12 center-align">
          <h1>Réservez ma coupe</h1>
        </div>
      </div>
      <div class="resa">
        <form action="control_client.php" method="post">
          <div id="date">
            <input type="text" class="datepicker" name="date" placeholder="choisir le jour de ma réservation" required/>
            <div id="hours">

            </div>
          </div>
          <div id="infos-perso">
            <label>Prénom :</label>
            <input type="text" name="prenom" placeholder="ex: Patrick" required>
            <label>Nom :</label>
            <input type="text" name="nom" placeholder="ex: Sébastien" required>
            <label>Email :</label>
            <input type="email" name="mail" placeholder="ex: psebastien@gmail.com" required>
            <label>Numéro de téléphone :</label>
            <input type="tel" name="tel" placeholder="ex: 0677201099" required>
            <input type="submit" name="sub" class="btn" value="Valider ma réservation">
          </div>

        </form>
      </div>

  </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="main.js"></script>
  </body>
</html>
