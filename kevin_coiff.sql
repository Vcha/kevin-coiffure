drop database if exists kevin_coiff;
create database kevin_coiff;
use kevin_coiff;

create table client (
  id int primary key auto_increment,
  nom varchar(254),
  prenom varchar(254),
  email varchar(254),
  tel int
);

create table rdv (
  id int primary key auto_increment,
  date_rdv date,
  heure_rdv time,
  id_client int,
  FOREIGN KEY (id_client) REFERENCES client(id),
  CONSTRAINT pair UNIQUE (date_rdv,heure_rdv)
);
