<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Partie Admin</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link rel="stylesheet" href="style.css">
</head>
<body>
<?php

include 'connectDB.php';

$date = $_POST['date'];

$reqSQL='Select * from rdv where date_rdv = :date';
$requete = $bdd->prepare($reqSQL);
$requete->bindValue(':date', $date);
$requete->execute();

$lesTuples = $requete->fetchAll();

 ?>
  <div class="container-fluid">
    <div class="row">
      <div class="col s12 center-align">
        <h1>Mes réservations</h1>
      </div>
    </div>
    <div class="resa row">
      <div class="col s12 l6">
        <form action="admin.php" method="post">
          <div id="date-resa">
            <input type="text" class="datepicker" name="date" placeholder="choisir le jour à visionner" required/>
            <input type="submit" name="sub" class="btn" value="Voir mes réservations">
          </div>
        </form>
      </div>

      <div id="infos-client" class="col s12 l6">
        <h2>Vos RDV pour le <?php echo $date ?></h2>
        <table class="responsive-table striped bordered">
          <thead>
            <tr>
              <th>Heure du RDV</th>
              <th>Nom du client</th>
              <th>Prénom du client</th>
              <th>email du client</th>
              <th>Numéro du client</th>
            </tr>
          </thead>

          <tbody>

            <?php
            if (!empty($lesTuples)){
              foreach ($lesTuples as $value) {
              ?>
              <tr>
                <?php
                $id_client = $value[3];
                $queryBdd = 'Select * from client where id = :id_client';
                $query = $bdd->prepare($queryBdd);

                $query->bindValue(':id_client', $id_client);
                $query->execute();

                $tab = $query->fetchAll();

                  foreach ($tab as $row): ?>
                  <td><?php echo $value[2]; ?></td>
                  <td><?php echo $row[1]; ?></td>
                  <td><?php echo $row[2]; ?></td>
                  <td><a href="mailto:<?php echo $row[3]; ?>"><?php echo $row[3]; ?></a></td>
                  <td><a href="tel:+33<?php echo "0".$row[4]; ?>"><?php echo "0".$row[4]; ?></a></td>
                <?php endforeach; ?>
              </tr>
              <?php
              }
            }
            else{
              ?>
                <p id="pasDeRdv">
                  Pas encore de RDV pour aujourd'hui !
                </p>
              <?php
            }
            ?>

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  <script src="admin.js"></script>
</body>
</html>
