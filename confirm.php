<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Kevin Coiffure</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="confirmation">
      <h1>
         Félicitation <?php echo $_SESSION["prenom"]." ".$_SESSION["nom"]; ?>,
         votre réservation du <?php echo $_SESSION["date"]." à ".$_SESSION["heure"]; ?>  a bien
         été prise en compte
       </h1>
       <a href="client.php" class="btn">Retourner sur la page de réservation</a>
    </div>

  </body>
</html>
