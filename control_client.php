<?php
session_start();
// Permet d'afficher les erreurs
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include 'connectDB.php';

$date = $_POST['date'];
$heure = $_POST['heure'];
$prenom = $_POST['prenom'];
$nom = $_POST['nom'];
$email = $_POST['mail'];
$tel = $_POST['tel'];

$tab = [$date, $heure, $prenom, $nom, $email, $tel];
var_dump($tab);

function insertClient($prenom, $nom, $email, $tel) {
  global $bdd;
  $reqSQL='
      INSERT INTO client (prenom, nom, email, tel)
      VALUES(:prenom, :nom, :email, :tel)
  ';
  $requete = $bdd->prepare($reqSQL);
  $requete->bindValue(':prenom', $prenom);
  $requete->bindValue(':nom', $nom);
  $requete->bindValue(':email', $email);
  $requete->bindValue(':tel', $tel);
  $requete->execute();
  $requete->closeCursor();
}

function insertRdv($date, $heure, $id_client) {
  global $bdd;
  $reqSQL='
      INSERT INTO rdv (date_rdv, heure_rdv, id_client)
      VALUES(:date_rdv, :heure_rdv, :id_client)
  ';
  $requete = $bdd->prepare($reqSQL);
  $requete->bindValue(':date_rdv', $date);
  $requete->bindValue(':heure_rdv', $heure);
  $requete->bindValue(':id_client', $id_client);
  $requete->execute();
  $requete->closeCursor();
}

 insertClient($prenom, $nom, $email, $tel);
 $last_id = $bdd->lastInsertId();
 var_dump($last_id);
 insertRdv($date, $heure, $last_id);

 $_SESSION["nom"] = $nom;
 $_SESSION["prenom"] = $prenom;
 $_SESSION["date"] = $date;
 $_SESSION["heure"] = $heure;

 header('Location: confirm.php');
 ?>
