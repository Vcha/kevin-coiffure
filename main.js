function dateEqual(date1, date2){
  if (date1.getFullYear()==date2.getFullYear()) {
    if (date1.getMonth()==date2.getMonth()) {
      if (date1.getDate()==date2.getDate()) {
        return true
      }
    }
  }
  return false
}


$(document).ready(function(){
  $('.datepicker').datepicker({
    format: "yyyy-mm-dd",
    minDate: new Date(),
    disableDayFn: function(date) {
      if(date.getDay() == 0 || date.getDay() == 1)
      return true;
      else
      return false;
    },
    onClose: function(date) {
      let formData = new FormData();
      let date_selectionee = $(".datepicker").val();
      formData.append('date', $(".datepicker").val());

      fetch('date.php', {

        header: {
          'Content-Type': 'application/json'
        },
        body: new URLSearchParams(formData),
        method: 'post'

      }).then(function(response) {
        if (response.status >= 200 && response.status < 300) {
          return response.json()
        }
        throw new Error(response.statusText)
      })
      .then(function(response) {

        let creneau_pris = [];
        let i = 0;
        for (i; i<response.length; i++) {
          let x = response[i][2];
          creneau_pris.push(x);
        }
        // response.map(function(creneau){
        //   creneau_pris.push(creneau[2])
        // })
        let vraidate = new Date(date_selectionee);
        let dateNow = new Date();
        if (vraidate.getDay() == 6){
          $('#hours').empty();
          var samedi = ['09:00:00', '10:00:00', '11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00', '17:00:00', '18:00:00'];
          for (let i = 0; i < samedi.length; i++) {
            let afficherSamedi = creneau_pris.includes(samedi[i]);

            if (dateEqual(vraidate, dateNow)) {
            let now = dateNow.getHours()+":00:00";

              if (!afficherSamedi && samedi[i]>now){
                $('#hours').append('<a class="btn" id="'+(i+1)+'">'+samedi[i]+'</a>')
              }
            }else{
              if (!afficherSamedi){
                $('#hours').append('<a class="btn" id="'+(i+1)+'">'+samedi[i]+'</a>')
              }
            }
          }


          $( "#hours .btn" ).on( "click", function() {
            var id = $(this).attr('id');
            if (id == 1) {
              $('#hours').append('<input type="hidden" name="heure" value="09:00">');
            }
            if (id == 2) {
              $('#hours').append('<input type="hidden" name="heure" value="10:00">');
            }
            if (id == 3) {
              $('#hours').append('<input type="hidden" name="heure" value="11:00">');
            }
            if (id == 4) {
              $('#hours').append('<input type="hidden" name="heure" value="12:00">');
            }
            if (id == 5) {
              $('#hours').append('<input type="hidden" name="heure" value="13:00">');
            }
            if (id == 6) {
              $('#hours').append('<input type="hidden" name="heure" value="14:00">');
            }
            if (id == 7) {
              $('#hours').append('<input type="hidden" name="heure" value="15:00">');
            }
            if (id == 8) {
              $('#hours').append('<input type="hidden" name="heure" value="16:00">');
            }
            if (id == 9) {
              $('#hours').append('<input type="hidden" name="heure" value="17:00">');
            }
            if (id == 10) {
              $('#hours').append('<input type="hidden" name="heure" value="18:00">');
            }
          });


        }else{
          $('#hours').empty();
          var semaine = ['09:00:00', '10:00:00', '11:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00', '17:00:00'];
          for (let i = 0; i < semaine.length; i++) {
            let afficherSemaine = creneau_pris.includes(semaine[i]);
            if (dateEqual(vraidate, dateNow)) {
            let now = dateNow.getHours()+":00:00";
              if (!afficherSemaine && semaine[i]>now){
                $('#hours').append('<a class="btn" id="'+(i+1)+'">'+semaine[i]+'</a>')
              }
            }else{
              if (!afficherSemaine){
                $('#hours').append('<a class="btn" id="'+(i+1)+'">'+semaine[i]+'</a>')
              }
            }
          }

          $( "#hours .btn" ).on( "click", function() {
            var id = $(this).attr('id');
            if (id == 1) {
              $('#hours').append('<input type="hidden" name="heure" value="09:00">');
            }
            if (id == 2) {
              $('#hours').append('<input type="hidden" name="heure" value="10:00">');
            }
            if (id == 3) {
              $('#hours').append('<input type="hidden" name="heure" value="11:00">');
            }
            if (id == 4) {
              $('#hours').append('<input type="hidden" name="heure" value="13:00">');
            }
            if (id == 5) {
              $('#hours').append('<input type="hidden" name="heure" value="14:00">');
            }
            if (id == 6) {
              $('#hours').append('<input type="hidden" name="heure" value="15:00">');
            }
            if (id == 7) {
              $('#hours').append('<input type="hidden" name="heure" value="16:00">');
            }
            if (id == 8) {
              $('#hours').append('<input type="hidden" name="heure" value="17:00">');
            }
          });
        }
      })
    }

  });

});
